/**
 * AdminController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

 const path = require('path')
 const fs   = require('fs')

module.exports = {
  
    home: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }else if (request.session.user.rol != 1) {
            return response.redirect('/')
        }

        let total_prod  = await Producto.count()
        let total_user  = await Cliente.count()
        let total_orden = await Facturacion.count()

        response.view('pages/admin/home', { total_prod, total_user, total_orden })
    },

    productos: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }else if (request.session.user.rol != 1) {
            return response.redirect('/')
        }

        const productos = await Producto.find().sort('id DESC')

        response.view('pages/admin/productos',{ productos })
    },

    agregarProducto: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }else if (request.session.user.rol != 1) {
            return response.redirect('/')
        }

        response.view('pages/admin/crear_producto')
    },

    crearProducto: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }else if (request.session.user.rol != 1) {
            return response.redirect('/')
        }

        let producto = await Producto.create({
            titulo: request.body.titulo,
            contenido: request.body.cantidad,
            precio: request.body.precio,
            cantidad: request.body.cantidad,
            activo: 1
        }).fetch()

        // agregar al inventario
        await Inventario.create({
            tipo: "ingreso de producto",
            producto: producto.id,
            cliente: request.session.user.id,
            cantidad: request.body.cantidad
        }).fetch()

        // cargar la foto
        request.file('foto').upload({}, async (err, files) => {
            if (files && files[0]) {
                let upload_path = files[0].fd
                let ext         = path.extname(upload_path)

                await fs.createReadStream(upload_path).pipe(fs.createWriteStream(path.resolve(sails.config.appPath, `assets/images/fotos/${producto.id}${ext}`)))
                await Producto.update({ id: producto.id },{ contenido: `${producto.id}${ext}` })
                
                request.addFlash('mensaje','Producto ingresado exitosamente')
                return response.redirect('/admin/agregar-producto')
            }

            request.addFlash('mensaje','No hay foto seleccionada')
            return response.redirect('/admin/agregar-producto')

        })
    },

};

