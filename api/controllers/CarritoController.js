/**
 * CarritoController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {

    agregar: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }

        const verifyProd    = await Orden.findOne({ producto: request.body.prod, cliente: request.session.user.id, activo: 1 })
        const prod          = await Producto.findOne({ id: request.body.prod })

        // descargamos la cantidad de productos seleccionados
        let nueva_cantidad = parseInt(prod.cantidad) - parseInt(request.body.cantidad)
        await Producto.update({ id: request.body.prod },{ cantidad: nueva_cantidad })

        if (verifyProd) {
            sails.log.warn(verifyProd.id)
            let cantidad    = parseInt(verifyProd.cantidad) + parseInt(request.body.cantidad)
            let updateProd  = await Orden.update({ id: verifyProd.id }, { cantidad: cantidad })
        }else{
            let orden = await Orden.create({ 
                cliente: request.session.user.id, 
                producto: request.body.prod, 
                cantidad: request.body.cantidad,
                activo: 1
            })

            const carrito                   = await Orden.find({ cliente: request.session.user.id, activo: 1 }).populate('producto')
            request.session.carrito         = carrito;
        }       

        // request.addFlash('mensaje',`Has agregado ${request.body.cantidad} ${prod.titulo} a tu carrito`)
        return response.redirect("/")
    },
  
    carrito: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }

        let total = 0;
        if (request.session.carrito.length) {
            request.session.carrito.forEach(detalle => {
                total += detalle.producto.precio * detalle.cantidad
            });
        }

        return response.view("pages/carrito", { total })
    },

    delete: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }

        const orden_delete  = await Orden.findOne({ id: request.params.id })
        const prod          = await Producto.findOne({ id: orden_delete.producto })

        // descargamos la cantidad de productos seleccionados
        let nueva_cantidad = parseInt(prod.cantidad) + parseInt(orden_delete.cantidad)
        await Producto.update({ id: orden_delete.producto },{ cantidad: nueva_cantidad })

        await Orden.destroy({ id: request.params.id })

        const carrito                   = await Orden.find({ cliente: request.session.user.id, activo: 1}).populate('producto')
        request.session.carrito         = carrito;

        request.addFlash('mensaje','El producto ha sido eliminado')
        return response.redirect("/carrito")
    },

    comprar: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }

        let total = 0;

        if (request.session.carrito.length) {
            request.session.carrito.forEach(detalle => {
                total += detalle.producto.precio * detalle.cantidad
                
            });

            let factura = await Facturacion.create({ 
                cliente: request.session.user.id, 
                total: total
            }).fetch()
            
            let orden = await Orden.update({ cliente: request.session.user.id, activo: 1 }, { activo: 0, factura: factura.id })

            
        }

        const carrito                   = await Orden.find({ cliente: request.session.user.id, activo: 1}).populate('producto')
        request.session.carrito         = carrito;

        request.addFlash('mensaje',`Su compra ha sido confirmada por un valor de: $${total}`)
        return response.redirect("carrito")
    },

    facturas: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }

        const facturas = await Facturacion.find({ cliente: request.session.user.id }).sort('id DESC')

        return response.view("pages/ordenes", { facturas })
    },

    orden: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }

        const factura = await Orden.find({ factura: request.params.id }).populate('producto')

        let total = 0;
        factura.forEach(deta => {
            total += deta.producto.precio * deta.cantidad
        })

        return response.view("pages/orden", { factura, total })
    },

};

