/**
 * ClienteController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    clientes: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }else if (request.session.user.rol != 1) {
            return response.redirect('/')
        }

        const clientes = await Cliente.find().sort('id DESC')

        response.view('pages/admin/clientes',{ clientes })
    },

    activar_cliente: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }else if (request.session.user.rol != 1) {
            return response.redirect('/')
        }

        const clientes = await Cliente.update({ id: request.params.id },{ activo: 1 })

        response.redirect('/admin/clientes')
    },

    desactivar_cliente: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }else if (request.session.user.rol != 1) {
            return response.redirect('/')
        }

        const clientes = await Cliente.update({ id: request.params.id },{ activo: 0 })

        response.redirect('/admin/clientes')
    },

    facturas: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }

        const cliente = await Cliente.findOne({ id: request.params.id })
        const facturas = await Facturacion.find({ cliente: request.params.id }).sort('id DESC')

        let nombre = cliente.nombre

        return response.view("pages/admin/cliente_ordenes", { facturas, nombre })
    },

    orden: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }

        const factura = await Orden.find({ factura: request.params.id }).populate('producto')

        let total = 0;
        factura.forEach(deta => {
            total += deta.producto.precio * deta.cantidad
        })

        return response.view("pages/admin/cliente_orden", { factura, total })
    },

};

