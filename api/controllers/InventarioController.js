/**
 * InventarioController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    inventario: async (request, response) => {
        if (!request.session || !request.session.user) {
            return response.redirect('/')
        }else if (request.session.user.rol != 1) {
            return response.redirect('/')
        }

        const inventarios = await Inventario.find().populate('producto').sort('id DESC')

        response.view('pages/admin/inventario',{ inventarios })
    },

};

