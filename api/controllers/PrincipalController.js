/**
 * PrincipalController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    home: async (request, response) => {

        const productos = await Producto.find({ activo: 1 })

        response.view('pages/homepage',  { productos })
    },

    about: async (request, response) => {
        response.view('pages/about')
    },

};

