/**
 * SesionController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  
    register: async (request, response) => {
        response.view('pages/registro')
    },

    procesar: async (request, response) => {
        let cliente = await Cliente.findOne({ email: request.body.email })

        if (cliente) {
            request.addFlash('mensaje','Email duplicado')
            return response.redirect("/registro")
        }else{
            let cliente = await Cliente.create({
                email: request.body.email,
                nombre: request.body.name,
                contrasena: request.body.contrasena
            })
            return response.redirect("/")
        }
    },

    login: async (request, response) => {
        response.view('pages/login')
    },

    procesarIngreso: async (request, response) => {
        let cliente = await Cliente.findOne({ email: request.body.email, contrasena: request.body.contrasena })

        if (cliente) {
            if (cliente.activo == 0) {
                request.addFlash('mensaje','Usuario inactivo, comuniquese con un administrador')
                return response.redirect("/ingresar")
            }

            request.session.authenticated   = true
            request.session.user            = cliente
            const orden                     = await Orden.find({ cliente: cliente.id, activo: 1 }).populate('producto')
            request.session.carrito         = orden;
            return response.redirect("/")
        }else{
            request.addFlash('mensaje','Credenciales incorrectas')
            return response.redirect("/ingresar")
        }
    },

    logout: async (request, response) => {
        request.session.destroy()
        return response.redirect("/")
    },

};

