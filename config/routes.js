/**
 * Route Mappings
 * (sails.config.routes)
 *
 * Your routes tell Sails what to do each time it receives a request.
 *
 * For more information on configuring custom routes, check out:
 * https://sailsjs.com/anatomy/config/routes-js
 */

module.exports.routes = {

  /***************************************************************************
  *                                                                          *
  * Make the view located at `views/homepage.ejs` your home page.            *
  *                                                                          *
  * (Alternatively, remove this and add an `index.html` file in your         *
  * `assets` directory)                                                      *
  *                                                                          *
  ***************************************************************************/

  'GET /': 'PrincipalController.home',

  'GET /acerca-de': 'PrincipalController.about',

  'GET /registro': 'SesionController.register',

  'POST /procesar-registro': 'SesionController.procesar',

  'GET /ingresar': 'SesionController.login',

  'POST /procesar-ingreso': 'SesionController.procesarIngreso',

  'GET /cerrar': 'SesionController.logout',

  'GET /carrito': 'CarritoController.carrito',

  'POST /agregar-producto': 'CarritoController.agregar',

  'GET /eliminar-prod/:id': 'CarritoController.delete',

  'GET /realizar-compra': 'CarritoController.comprar',

  'GET /mis-ordenes': 'CarritoController.facturas',

  'GET /orden/:id': 'CarritoController.orden',

  // rutas de administrador

  'GET /admin/panel': 'AdminController.home',

  'GET /admin/productos': 'AdminController.productos',

  'GET /admin/agregar-producto': 'AdminController.agregarProducto',

  'POST /admin/crear-producto': 'AdminController.crearProducto',

  'GET /admin/inventario': 'InventarioController.inventario',

  'GET /admin/clientes': 'ClienteController.clientes',

  'GET /admin/administradores': 'ClienteController.administradores',

  'GET /admin/cliente/activar/:id': 'ClienteController.activar_cliente',

  'GET /admin/cliente/desactivar/:id': 'ClienteController.desactivar_cliente',

  'GET /admin/cliente/ordenes/:id': 'ClienteController.facturas',

  'GET /admin/cliente/orden/:id': 'ClienteController.orden',

  /***************************************************************************
  *                                                                          *
  * More custom routes here...                                               *
  * (See https://sailsjs.com/config/routes for examples.)                    *
  *                                                                          *
  * If a request to a URL doesn't match any of the routes in this file, it   *
  * is matched against "shadow routes" (e.g. blueprint routes).  If it does  *
  * not match any of those, it is matched against static assets.             *
  *                                                                          *
  ***************************************************************************/


};
